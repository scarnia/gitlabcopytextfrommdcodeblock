// ==UserScript==
// @name        Gitlab Copy Code Snippet
// @version     0.0.1
// @description A userscript adds a copy to clipboard button on hover of markdown code snippets in Gitlab
// @license     MIT
// @author      Carlos Leyva (Original Author Rob Garrison)
// @namespace   https://gitlab.com/scarnia/gitlabcopytextfrommdcodeblock
// @include     https://gitlab.com/*
// @run-at      document-idle
// @grant       GM_addStyle
// @require https://cdn.rawgit.com/zenorocha/clipboard.js/v1.5.5/dist/clipboard.min.js
// @icon        https://github.githubassets.com/pinned-octocat.svg
// ==/UserScript==
(() => {
	"use strict";

	let copyId = 0;
	const markdownSelector = ".file-content, .md", codeSelector = "pre, .code", copyButton = document.createElement("clipboard-copy");
	copyButton.className = "btn btn-sm tooltipped tooltipped-w gh-csc-button";
	copyButton.setAttribute("aria-label", "Copy to clipboard");
	// This hint isn't working yet (GitHub needs to fix it)
	copyButton.setAttribute("data-copied-hint", "Copied!");
	copyButton.innerHTML = `
		<svg aria-hidden="true" class="octicon octicon-clippy" height="16" viewBox="0 0 14 16" width="14">
			<path fill-rule="evenodd" d="M2 13h4v1H2v-1zm5-6H2v1h5V7zm2 3V8l-3 3 3 3v-2h5v-2H9zM4.5 9H2v1h2.5V9zM2 12h2.5v-1H2v1zm9 1h1v2c-.02.28-.11.52-.3.7-.19.18-.42.28-.7.3H1c-.55 0-1-.45-1-1V4c0-.55.45-1 1-1h3c0-1.11.89-2 2-2 1.11 0 2 .89 2 2h3c.55 0 1 .45 1 1v5h-1V6H1v9h10v-2zM2 5h8c0-.55-.45-1-1-1H8c-.55 0-1-.45-1-1s-.45-1-1-1-1 .45-1 1-.45 1-1 1H3c-.55 0-1 .45-1 1z"></path>
		</svg>`;

	GM_addStyle(`
		.gh-csc-wrap {
			position: relative;
		}
		.gh-csc-wrap:hover .gh-csc-button {
			display: block;
		}
		.gh-csc-button {
			display: none;
			padding: 3px 6px;
			position: absolute;
			top: 3px;
			right: 3px;
			z-index: 20;
		}
		.gh-csc-wrap.ghd-code-wrapper .gh-csc-button {
			right: 31px;
		}
		.gh-csc-button svg {
			vertical-align: text-bottom;
		}
	`);

	function addButton(wrap, code) {
		if (!wrap.classList.contains("gh-csc-wrap")) {
			//console.log("1 detected");
			copyId++;
			// See comments from sindresorhus/refined-github/issues/1278
			code.id = `gh-csc-${copyId}`;
			copyButton.setAttribute("for", `gh-csc-${copyId}`);
			wrap.classList.add("gh-csc-wrap");
			wrap.insertBefore(copyButton.cloneNode(true), wrap.childNodes[0]);
		}
	}

	function init() {
        if (!GitlabCopyScriptLoaded) {
            const markdown = document.querySelector(markdownSelector);
            if (markdown) {
                [...document.querySelectorAll(markdownSelector)].forEach(md => {
                    [...md.querySelectorAll(codeSelector)].forEach(pre => {
                        let code = pre.querySelector("code");
                        let wrap = pre.parentNode;
                        if (code) {
                            // pre > code
                            addButton(pre, code);
                        } else if (wrap.classList.contains("highlight")) {
                            // div.highlight > pre
                            addButton(wrap, pre);
                        }
                    });
                });
                    new Clipboard('.gh-csc-button', {
                        text: function(trigger) {
                            debugger;
                            return $(trigger.nextElementSibling).text();
                        }
                    })
                        .on('success',function (e) {$e.trigger.setAttribute("title", "\\done@copy");})
                        .on('error',function (e) {$e.trigger.setAttribute("title", "\\done@copy");});

                GitlabCopyScriptLoaded = true;
            }
        }
	}
	//console.log("Script called");
    setTimeout(() => {
        //console.log("Timeout reached");
        init();
    }, 3000);
    var GitlabCopyScriptLoaded = false;

})();

