# Introduction

This simple js script takes care of adding a button to the blocks of code inside MarkDown documents in Gitlab. Although this functionality is supposed to be alredy included by Gitlab, it seems that it only works in the readme file of each project, and not when navigating the repository. See:
- https://gitlab.com/gitlab-org/gitlab/-/issues/349134
- https://gitlab.com/gitlab-org/gitlab/-/issues/21172


To overcome this, I created a new script (most properly, modified an already existing one by Rob Garrison's developments for Gitlab (https://github.com/Mottie/GitHub-userscripts)). The original script made use of Github already builtin functionality for copying content (which here is implemented using ClipboardJs and was targetting github elements). Please feel free to fork or improve as this was a quick and dirty implementation to ease some interactive deployment work I needed to do (and that implied copying a lot of code from MD documents).

To install it, use GreaseMonkey or TamperMonkey
